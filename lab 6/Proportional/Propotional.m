% Lab 6 Proportional Fan Control

clear all
close all

% Load Data

load a1_prop.mat;
load a2_prop.mat;
load a3_prop.mat;
load b1_prop.mat;
load c1_prop.mat;
load c2_prop.mat;

% Set variable Info

t1a = a1_prop.X.Data;
temp_1a = a1_prop.Y(1,1).Data;
v1a = a1_prop.Y(1,2).Data;

t1b = b1_prop.X.Data;
temp_1b = b1_prop.Y(1,1).Data;
v1b = b1_prop.Y(1,2).Data;

t1c = c1_prop.X.Data;
temp_1c = c1_prop.Y(1,1).Data;
v1c = c1_prop.Y(1,2).Data;

t2a = a2_prop.X.Data;
temp_2a = a2_prop.Y(1,1).Data;
v2a = a2_prop.Y(1,2).Data;

t2b = a1_prop.X.Data;
temp_2b = a1_prop.Y(1,1).Data;
v2b = a1_prop.Y(1,2).Data;

t2c = c2_prop.X.Data;
temp_2c = c2_prop.Y(1,1).Data;
v2c = c2_prop.Y(1,2).Data;

t3a = a3_prop.X.Data;
temp_3a = a3_prop.Y(1,1).Data;
v3a = a3_prop.Y(1,2).Data;

t3b = a1_prop.X.Data;
temp_3b = a1_prop.Y(1,1).Data;
v3b = a1_prop.Y(1,2).Data;

% Plot Info

figure(1) % Effect of Kp

subplot(3,1,1)
plot(t1a,v1a)
hold on;
plot(t1b,v1b, 'r')
hold on;
plot(t1c,v1c,'g')
ylabel('Voltage');

subplot(3,1,[2 3])
plot(t1a,temp_1a);
hold on;
plot(t1b,temp_1b, 'r');
hold on;
plot(t1c,temp_1c, 'g');
xlabel('Time');
ylabel('Temperature (*C)')

legend('kp = 1','kp = 2', 'kp = 3', 'Location', 'SouthEast')

figure(2) % Effect of Set Point

subplot(3,1,1)
plot(t2a,v2a)
hold on;
plot(t2b,v2b, 'r')
hold on;
plot(t2c,v2c,'g')
ylabel('Voltage');

subplot(3,1,[2 3])
plot(t2a,temp_2a);
hold on;
plot(t2b,temp_2b, 'r');
hold on;
plot(t2c,temp_2c, 'g');
xlabel('Time');
ylabel('Temperature (*C)')

legend('60*C', '70*C', '80*C', 'Location', 'SouthEast')

figure(3) % Effect of power level

subplot(3,1,1)
plot(t3a,v3a)
hold on;
plot(t3b,v3b, 'r')


subplot(3,1,[2 3])
plot(t3a,temp_3a);
hold on;
plot(t3b,temp_3b, 'r');
xlabel('Time');
ylabel('Temperature (*C)')

legend('Low Power', 'High Power', 'Location', 'SouthEast')