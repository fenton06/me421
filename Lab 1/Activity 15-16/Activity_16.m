% Activity_16

time = activity_15.X(1,1).Data;
var1 = activity_15.Y(1,1).Data;
var2 = activity_15.Y(1,2).Data;

plot(time, var1, 'g')
title('Activity 16 (Sawtooth)')
xlabel('Time (s)')
ylabel('Frequency (Hz)')