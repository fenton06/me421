% Activity 11

clear all
close all

constant = 4;

t = (0:.1:10)';
u = linspace(constant,constant,101)';

input = [t u];

sim('Activity_11');

y = output.signals.values(:,1);

plot(tout,y);
title('Activity 11')
xlabel('Time')