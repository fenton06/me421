% Activity 12

clear all
close all

sim('Activity_12');

y = output.signals.values(:,1);

plot(tout,y);
title('Activity 12')
xlabel('Time');
ylabel('Voltage');